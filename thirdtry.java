package ubeytemenya;

public class HellOK {
    private static void salary(double salary, double hours) {
        if (hours > 60) {
            hours = 60;
        }
        if (hours > 40) {
            hours = 40 + (hours - 40) * 1.5;
        }
 
        if (salary < 8) {
            salary = 8;
        }
         java.io.PrintStream ps = null;
        try {
            ps = new java.io.PrintStream("salary.txt", "UTF-8");
            ps.print(String.format("salary: %.2f hours: % .0f  result: %.2f", salary, hours, (salary * hours)));
        } catch (Exception e) {
            throw new RuntimeException("File Error");
        }finally {
            ps.close();
        }
 
    }
 
 
    public static void main(String[] args) {
 
        salary(12, 10);
        salary(4, 24);
        salary(42, 75);
 
    }
}
